<?php
  
  class form 
  {
    // list of domains returned back to client  
    private $domainArray = array();
    
    // return array  of domains
    public function returnArrayOfDomains()
    {
        return $this->domainArray;
    }
    
    // separating email inputs by "whitespace" 
    //...and placing them into an array  
    public function separateEmailInputs($emailInputs) 
    {
        $emailsArray = explode(" ", $emailInputs);
        return $emailsArray;
    }
      
    // looping through the emailsArray, retrieving all existing and 
    //...unique domains, and placing them into the dominList array
    public function uniqueDomainRetrieval($emailsArray)
    {
        
        $lenghtOfEmailsArray = count($emailsArray);
        $positionOfDomain = '';
        $domainOfEmail = '';
        for($x = 0; $x <= $lenghtOfEmailsArray - 1; $x++)
        {
           $positionOfDomain = strpos($emailsArray[$x], '@');
           // verifying that a domain and been inputed
           if($positionOfDomain)
           {
             $domainOfEmail = substr($emailsArray[$x], $positionOfDomain + 1);
               
             // verifying that no repeated domains are entered into the domainArray
             if(!in_array($domainOfEmail, $this->domainArray)) 
             {
                array_push($this->domainArray, $domainOfEmail);
             }
           }
        }   
    } 
  }
?>    
           
<?php

//verifying that request was a post request
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
    //retrieving input data sent from client
    $json = file_get_contents('php://input');
    $data = json_decode($json, true);
    
    //setting contet-type for data being sent back to client
    header('Content-Type: application/json');
    
    if($data['emails'] == '')
    {
        //sending back error response if any required input fields are empty
        $data = "Please fill in all required input fields, thank you!";
        echo json_encode($data);
        die();                       
    }
    
    //creating a new form object, passing in our email inputs, and retrieving their unique domains
    $enteredForm = new form();
    $emailsArray = $enteredForm->separateEmailInputs($data['emails']);
    $enteredForm->uniqueDomainRetrieval($emailsArray);
    $domainArray = $enteredForm->returnArrayOfDomains();
    echo json_encode($domainArray);
    die();
 
}

?>

<html>

<head>
    
<script src="//code.angularjs.org/1.5.0-rc.0/angular.js"></script>
               
<style type="text/css">
    
.emailForm
{
    max-width: 50%;
    margin: 15% 25%;
    font: 13px Arial, Helvetica, sans-serif;
}
    
.emailForm-heading
{
    font-weight: bold;
    font-style: italic;
    border-bottom: 2px solid #ddd;
    margin-bottom: 20px;
    font-size: 15px;
    padding-bottom: 3px;
}
    
.emailForm label
{
    display: block;
    margin: 0px 0px 15px 0px;
}
    
.emailForm label > span
{
    width: 100px;
    font-weight: bold;
    float: left;
    padding-top: 8px;
    padding-right: 5px;
}
    
.emailForm span.required{
    color:red;
}
      
.emailForm .textarea-field
{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #C2C2C2;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 7px;
    outline: none;
}

.emailForm .textarea-field:focus,  
{
    border: 1px solid #0C0;
}
    
.emailForm .textarea-field{
    height:100px;
    width: 55%;
}
    
.emailForm input[type=submit],
.emailForm input[type=button]
{
    border: none;
    padding: 8px 15px 8px 15px;
    background: #FF8500;
    color: #fff;
    box-shadow: 1px 1px 4px #DADADA;
    -moz-box-shadow: 1px 1px 4px #DADADA;
    -webkit-box-shadow: 1px 1px 4px #DADADA;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
}
    
.emailForm input[type=submit]:hover,
.emailForm input[type=button]:hover
{
    background: #EA7B00;
    color: #fff;
}
    
.domainList
{
    text-align: center;
    list-style-position: inside;
    font-weight: bold;
    font-style: italic;
    font-size: 15px;
}
    
</style>
     
</head>

<body ng-app = "myApp">

  <div class="emailForm" ng-controller="myCtrl" ng-submit="sendEmail()">
    <div class="emailForm-heading">ASCI Code Challenge</div>
      <form>
        <label for="emails">
          <span>Emails<span class="required">*</span></span>
          <textarea ng-model='emails.input' class="textarea-field"></textarea>
        </label>
        <label><span>&nbsp;</span><input type="submit" value="Submit" /></label>
      </form>
      <ol class="domainList">
        <li ng-repeat="x in domainList">
          {{ x }}
        </li>
      </ol>   
  </div>

</body>
    
<script>
    
var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $http) 
{
  // values of your form's input fields
  $scope.emails = {input: ''};
  $scope.domainList = '';
  
  // function that is ran upon form entry 
  $scope.sendEmail = function() {
       
    $http.post("asciCodeChallenge.php", { 'emails' : $scope.emails.input}
    )       
      //response sent back from the server
      .then(function(response) 
      {  
        $scope.domainList = response.data;   
      });  
  }; 
});
  
</script>
    
</html>


